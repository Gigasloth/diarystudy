package com.app.gigasloth.diarystudy.model;


import java.util.ArrayList;

public class Study {
    private String title;
    private String instructions;
    private ArrayList<Survey> surveys = new ArrayList<>();
    // Pin Code for participant access to the study
    private int pinCode;

    public Study(String title, String instructions, ArrayList<Survey> surveys, int pinCode) {
        this.title = title;
        this.instructions = instructions;
        this.surveys = surveys;
        this.pinCode = pinCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public ArrayList<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(ArrayList<Survey> surveys) {
        this.surveys = surveys;
    }

    public void addSurvey(Survey survey) {
        surveys.add(survey);
    }

    public void removeSurvey(Survey survey) {
        surveys.remove(survey);
    }

    public void removeSurveyAt(int index) {
        surveys.remove(index);
    }

    public void clearSurverys() {
        surveys.clear();
    }

    // TODO: implement with firebase. associate study with researcher
    public void save() {
    }

    public void delete() {
    }
}
