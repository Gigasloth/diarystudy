package com.app.gigasloth.diarystudy.model;

public class Researcher extends User {
    // arraylist of studies created by the researcher
    // a pin code assigned to the study will associate it with it's users

    public Researcher(String name, String email) {
        super(name, email);
    }
}
