package com.app.gigasloth.diarystudy.model;


public class MultipleChoiceQuestion extends Question {
    private String response;

    public MultipleChoiceQuestion(boolean required, String text, String response) {
        super(required, text);
        this.response = response;
        setType(Type.MULTIPLE_CHOICE);
    }

    public boolean hasValidResponse() {
        return isRequired() && !response.isEmpty();
    }
}
