package com.app.gigasloth.diarystudy.model;


import java.util.ArrayList;

public class MultipleSelectQuestion extends Question {
    private ArrayList<String> responses;

    public MultipleSelectQuestion(boolean required, String text, ArrayList<String> responses) {
        super(required, text);
        this.responses = responses;
        setType(Type.MULTIPLE_SELECT);
    }

    public boolean hasValidResponse() {
        return isRequired() && !responses.isEmpty();
    }
}
