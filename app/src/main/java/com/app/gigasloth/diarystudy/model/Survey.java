package com.app.gigasloth.diarystudy.model;


import java.util.ArrayList;

public class Survey {
    private String title;
    private String instructions;
    private ArrayList<Question> questions = new ArrayList<>();

    public Survey(String title, String instructions, ArrayList<Question> questions) {
        this.title = title;
        this.instructions = instructions;
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public void removeQuestion(Question question) {
        questions.remove(question);
    }

    public void removeQuestionAt(int index) {
        questions.remove(index);
    }

    public void clearQuestions() {
        questions.clear();
    }

    // TODO: implement with firebase. associate survey with researcher
    public void save() {
    }

    public void delete() {
    }
}
