package com.app.gigasloth.diarystudy.model;


public class OpenEndedQuestion extends Question {
    private String response;

    public OpenEndedQuestion(boolean required, String text, String response) {
        super(required, text);
        this.response = response;
        setType(Type.OPEN_ENDED);
    }

    public boolean hasValidResponse() {
        return isRequired() && !response.isEmpty();
    }
}
