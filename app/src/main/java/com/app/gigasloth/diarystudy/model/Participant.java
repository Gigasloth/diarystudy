package com.app.gigasloth.diarystudy.model;

import java.util.ArrayList;

public class Participant extends User {
    // 4 Digit pin code to associate participant with studies they can access
    ArrayList<Integer> pinCodes = new ArrayList<>();

    public Participant(String name, String email) {
        super(name, email);
    }

    public ArrayList<Integer> getPinCodes() {
        return pinCodes;
    }

    public void setPinCodes(ArrayList<Integer> pinCodes) {
        this.pinCodes = pinCodes;
    }

    public void addPinCode(int pinCode) {
        pinCodes.add(pinCode);
    }
}
