package com.app.gigasloth.diarystudy.model;


public abstract class Question {
    private boolean required;
    private String text;
    private Type type;

    public enum Type {
        MULTIPLE_CHOICE,
        MULTIPLE_SELECT,
        OPEN_ENDED
    }

    public Question(boolean required, String text) {
        this.required = required;
        this.text = text;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    // Question contract. Valid responses may vary depending on question type.
    public abstract boolean hasValidResponse();
}
