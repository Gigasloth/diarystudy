[![](https://jitpack.io/v/org.bitbucket.gigasloth/diarystudy.svg)](https://jitpack.io/#org.bitbucket.gigasloth/diarystudy)

# README #

This app is a diary study for UX research. Requirements are defined in https://docs.google.com/document/d/1BfViVffLX6H1B6FdyHJOt0dpf2T4R4aRM-ojMnMrDTE/edit

### How do I get set up? ###

Run as is.

### Who do I talk to? ###

* Repo owner or admin
Gigasloth